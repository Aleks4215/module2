using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;


namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Program program = new Program();
            program.GetCongratulation(14);
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            if(companiesNumber > 0 && tax > 0 && companyRevenue > 0)
            {
                int oneCompany = companyRevenue / tax;
                int totalTax = oneCompany * companiesNumber;
                return totalTax;
            } else
            {
                return 0;
            }
        }

        public string GetCongratulation(int input)
        {
            if(input%2 == 0 && input >= 18)
            {
                return "Поздравляю с совершеннолетием!";
            }
            else if (input % 2 != 0 && input > 12 && input < 18)
            {
                return "Поздравляю с переходом в старшую школу!";
            }
            return $"Поздравляю с {input}-летием!";
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            double dFirst = 0.0;
            double dSecond = 0.0;

            if(first.Contains('.') && first.Contains(',') && second.Contains('.') && second.Contains(','))
            { 
                var a = double.TryParse(first, out dFirst);
                var b = double.TryParse(second, out dSecond);
                if(a && b)
                {
                    dFirst = double.Parse(first);
                    dSecond = double.Parse(second);
                }
                return dFirst * dSecond;
            } else
            {
                var a = double.TryParse(first, out dFirst);
                var b = double.TryParse(second, out dSecond);
                if (a && b)
                {
                    dFirst = double.Parse(first);
                    dSecond = double.Parse(second);
                }
                return dFirst * dSecond;
            }
        }

        public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            
            if(figureType == Figure.Circle)
            {
                double pi = 3.1415;
                if (parameterToCompute == Parameter.Perimeter)
                {
                    if (dimensions.Radius > 0)
                    {
                        return Math.Round(2 * pi * dimensions.Radius, MidpointRounding.AwayFromZero);
                    }
                }
                else
                {
                    if (dimensions.Radius > 0)
                    {
                        return Math.Round(pi * (dimensions.Radius * dimensions.Radius), MidpointRounding.AwayFromZero);
                    }
                }
            }
            if (figureType == Figure.Rectangle)
            {
                if (parameterToCompute == Parameter.Perimeter)
                {
                    if (dimensions.FirstSide > 0 && dimensions.SecondSide > 0)
                    {
                        return Math.Round((dimensions.FirstSide + dimensions.SecondSide) * 2, MidpointRounding.AwayFromZero);
                    }
                }
                else
                {
                    if (dimensions.FirstSide > 0 && dimensions.SecondSide > 0)
                    {
                        return Math.Round(dimensions.SecondSide * dimensions.SecondSide, MidpointRounding.AwayFromZero);
                    }
                }
            }
            if (figureType == Figure.Triangle)
            {
                if (parameterToCompute == Parameter.Perimeter)
                {
                    if (dimensions.FirstSide > 0 && dimensions.SecondSide > 0 && dimensions.ThirdSide > 0)
                    {
                        return Math.Round(dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide, MidpointRounding.AwayFromZero);
                    }
                }
                else
                {
                    if (dimensions.FirstSide > 0 && dimensions.Height > 0)
                    {
                        return Math.Round(1 / 2 * dimensions.Height * dimensions.FirstSide, MidpointRounding.AwayFromZero);
                    }
                }
            }
            return 0.0;
        }
    }
}
